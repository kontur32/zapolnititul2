from flask import Flask, request, jsonify, send_file
import boto3
import os

app = Flask(__name__)

# получение параметров из окружения
AWS_ACCESS_KEY_ID = os.environ['AWS_ACCESS_KEY_ID']
AWS_SECRET_ACCESS_KEY = os.environ['AWS_SECRET_ACCESS_KEY']
AWS_REGION = os.environ['AWS_REGION']
AWS_ENDPOINT_URL = os.environ['AWS_ENDPOINT_URL']

# инициализация клиента S3
s3 = boto3.client('s3', aws_access_key_id=AWS_ACCESS_KEY_ID,
                         aws_secret_access_key=AWS_SECRET_ACCESS_KEY,
                         region_name=AWS_REGION,
                         endpoint_url=AWS_ENDPOINT_URL)

@app.route('/s3/buckets/<bucket_name>/object', methods=['PUT', 'GET', 'DELETE'])
def object_crud(bucket_name):
    object_id = request.args.get('id')
    if request.method == 'PUT':
        # создание или обновление объекта
        obj = request.get_data()
        s3.put_object(Body=obj, Bucket=bucket_name, Key=object_id)
        return jsonify({'message': 'Object created/updated successfully'})
    elif request.method == 'GET':
        # чтение объекта
        obj = s3.get_object(Bucket=bucket_name, Key=object_id)
        return send_file(obj['Body'], mimetype=obj['ContentType'])
    elif request.method == 'DELETE':
        # удаление объекта
        s3.delete_object(Bucket=bucket_name, Key=object_id)
        return jsonify({'message': 'Object deleted successfully'})

@app.route('/s3/buckets/<bucket_name>/list', methods=['GET'])
def list_objects(bucket_name):
    prefix = request.args.get('prefix')
    if prefix:
        objects = s3.list_objects(Bucket=bucket_name, Prefix=prefix)
    else:
        objects = s3.list_objects(Bucket=bucket_name)
    object_list = []
    for obj in objects.get('Contents', []):
        object_list.append({'id': obj['Key'], 'size': obj['Size']})
    if not object_list:
        return jsonify({'objects': []})
    return jsonify({'objects': object_list})


if __name__ == '__main__':
    app.run(Port=5005, Debug=True)
