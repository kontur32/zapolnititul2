// отправляет данные формы POST-запросом по указанному адресу
function sendForm(formId) {
    const form = document.getElementById(formId);
  
    form.addEventListener('submit', (event) => {
      event.preventDefault();
  
      const formData = new FormData(form);

      //const cookies = document.cookie.split(';');
      
      const xhr = new XMLHttpRequest();
      xhr.open('POST', form.action, true);
  
      xhr.send(formData);
  
      xhr.onload = () => {
        if (xhr.status === 200) {
          location.reload(); // перезагружает страницу 
          console.log('Форма отправлена успешно!');
          console.log('Ответ сервера:', xhr.responseText);
        } else {
          console.error('Ошибка отправки формы:', xhr.statusText);
        }
      };
    });
};
  
// вешает функццию отправки данных на форму загрузки шаблона
$(document).ready(function() {
    sendForm('upload_file');
});

// устанавливает значение checked соответствующему полю статуса документа
$(document).ready(function() {
  const form = document.getElementById('upload_file');
  const statusElement = form.querySelector('#status');
  const statusValue = statusElement.getAttribute('value');
   
  const inputElement = statusElement.querySelector(`input[id="${statusValue}"]`);
  if (inputElement) {
    inputElement.setAttribute('checked', 'yes');
  }
});