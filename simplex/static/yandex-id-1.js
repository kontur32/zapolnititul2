window.onload = function() { 

   const yaIdElement = document.getElementById('ya-id');
   if (yaIdElement) {
      window.YaAuthSuggest.init({
                  client_id: 'c4b0d164191c4b56a9f224c89a7f3116',
                  response_type: 'token',
                  redirect_uri: 'https://zt.titul24.ru/static/ya-response.html'
               },
               'https://zt.titul24.ru/', {
                  view: 'button',
                  parentId: 'ya-id',
                  buttonView: 'main',
                  buttonTheme: 'light',
                  buttonSize: 'm',
                  buttonBorderRadius: 0
               }
            )
            .then(function(result) {
               return result.handler()
            })
            .then(function(data) {
               console.log('Сообщение с токеном: ', data);

               const accessToken = data.access_token;
               const redirectUrl = 'https://zt.titul24.ru/';
               const params = `access_token=${accessToken}`;

               window.location.href = `${redirectUrl}?${params}`;
            })
            .catch(function(error) {
               console.log('Что-то пошло не так: ', error);
            });
      };
      
};
    
