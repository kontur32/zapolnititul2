module namespace login = "public/login";

declare 
  %private
function login:main($params as map(*)) as map(*){
  let $access_token := request:parameter("access_token")
  let $m :=
    if($access_token)then($access_token)else()
  
  let $user :=
    if($m)
    then( 
      (web:create-url(
        "https://login.yandex.ru/info",
        map{
          "oauth_token": $m,
          "format":"xml"
        }
      )=>fetch:xml())/user
    )
    else()
  
  let $session :=
    if($user/id/text())
    then(
      session:set("login", $user/default_email/text()),
      session:set("id", $user/id/text())
    )
    else()
  return
    map{
      "user":$user,
      "login":session:get("login"),
      "is_login":if(session:get("login"))then()else("ya-id")
    }
};