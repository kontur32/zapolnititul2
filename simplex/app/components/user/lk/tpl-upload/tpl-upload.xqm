module namespace body = "user/lk/tpl-upload";
import module namespace sf="http://www.iro37.ru/simplex-framework";

declare 
  %private
function body:main($params as map(*)) as map(*){
  
  let $properties as xs:base64Binary := 
    sf:lib('s3', 'getObjeckt', map{'label': "properties/" || session:get("id") || "/" || request:parameter("label") || "/properties.xml"})
  
  return
    map{
      "статус": ($properties => convert:binary-to-string() => parse-xml())//status/text(),
      "шаблон": request:parameter("label")
    }
};