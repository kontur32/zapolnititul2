module namespace body = "user/lk/tpl-form/links";
import module namespace sf="http://www.iro37.ru/simplex-framework";

declare 
  %private
function body:main($params as map(*)) as map(*){
  map{
    "названиеШаблона": $params?label
  }
};