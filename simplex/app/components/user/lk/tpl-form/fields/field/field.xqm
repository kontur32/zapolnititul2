module namespace body = "user/lk/tpl-form/fields/field";
import module namespace sf="http://www.iro37.ru/simplex-framework";

declare 
  %private
function body:main($params as map(*)) as map(*){
  map{
    "названиеПоля": $params?field/entry[1]/text() => normalize-space(),
    "имяПоля": $params?field/entry[1]/text() => normalize-space()
  }
};