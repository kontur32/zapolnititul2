module namespace body = "user/lk/tpl-form/fields";
import module namespace sf="http://www.iro37.ru/simplex-framework";

declare 
  %private
function body:main($params as map(*)) as map(*){
  let $templateLabel := request:parameter("label")
  let $template as xs:base64Binary := 
    sf:lib('s3', 'getObjeckt', map{'label': session:get("id") || "/" || $templateLabel})
  let $args :=
    map{
      "method":"POST",
      "url": 
        web:create-url(
          "http://zt2-ooxml-1:8984/ooxml/api/v1/docx/fields2",
          map{}
        ),
      "payload": $template
    }
  let $send := 
    try{
      sf:lib('http.client', 'post', $args)[2] => csv:parse(map{"separator":";"})
    }catch*{"ошибка"}
  return
    map{
      "имяШаблона": $templateLabel,
      "поляФормы": $send//record ! sf:tpl('user/lk/tpl-form/fields/field', map{"field":.})
    }
};