module namespace body = "user/lk/tpl-form";
import module namespace sf="http://www.iro37.ru/simplex-framework";

declare 
  %private
function body:main($params as map(*)) as map(*){
  map{
    "шаблон": request:parameter("label"),
    "ссылки": sf:tpl('user/lk/tpl-form/links', map{"label": request:parameter("label")}),
    "поля": sf:tpl('user/lk/tpl-form/fields', map{})
  }
};