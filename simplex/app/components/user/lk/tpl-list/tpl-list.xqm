module namespace body = "user/lk/tpl-list";
import module namespace sf="http://www.iro37.ru/simplex-framework";

declare 
  %private
function body:main($params as map(*)) as map(*){
  let $url :=
    web:create-url(
      'http://zt2-aws-1:5000/s3/buckets/zt-templates/list',
      map{
        "prefix": session:get("id")
      }
    )=> fetch:text() => json:parse()
  let $list := 
    distinct-values($url//id/tokenize(text(), "/")[2])
  let $h :=
    $list ! sf:tpl('user/lk/tpl-list/item', map{"fileName": .})
  return
    map{
      "список": $h
    }
};