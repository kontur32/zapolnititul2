module namespace body = "user/lk2";
import module namespace sf="http://www.iro37.ru/simplex-framework";

declare 
  %private
function body:main($params as map(*)) as map(*){
  map{
    "списокШаблонов": sf:tpl('user/lk/tpl-list', map{}),
    "формаШаблона": sf:tpl('user/lk/tpl-form', map{}),
    "загрузитьШаблон": sf:tpl('user/lk/tpl-upload', map{})
  }
};