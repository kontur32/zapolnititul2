module namespace main = "main";
import module namespace sf="http://www.iro37.ru/simplex-framework";

declare function main:main($params as map(*)){
    map{
      'header' : sf:tpl("main/header", map{}),
      'body' : $params?body,
      'footer' : sf:tpl("main/footer", map{})
    }
};