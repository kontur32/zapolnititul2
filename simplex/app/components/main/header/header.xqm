module namespace header = "main/header";
import module namespace sf="http://www.iro37.ru/simplex-framework";

declare function header:main( $params as map(*) ){
  map{
    'mainMenu' : sf:tpl('main/header/mainMenu', map{}),
    'меню': session:get("login"),
    'выйти': 
      if(session:get("login"))
      then(
        <a class="btn btn-info mt-3" href="/logout">Выйти</a>
      )
      else()
  }  
};