module namespace body = "main/body";
import module namespace sf="http://www.iro37.ru/simplex-framework";

declare 
  %private
function body:main($params as map(*)) as map(*){
  let $body :=
    if($params?page)
    then($params?page, $params)
    else(
      <div>
        <a href="/admin/pages.list">Список страниц</a> <br/>
        <a href="/u/class.shedule.week/" class="btn btn-info" role="button">Расписание класса</a>
        <a href="/u/teacher.shedule.week/" class="btn btn-info" role="button">Расписание учителя</a>
      </div>
    )
  return
    map{
      "привет": sf:lib('funct', 'tmp', map{"name": "Андрей"}),
      "контент": $body
    }
};