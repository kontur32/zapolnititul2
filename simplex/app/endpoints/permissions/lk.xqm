module namespace prerm = "zapolnititul2/v1.0/user/lk/permissions";

declare 
  %perm:check('/lk', '{$perm}')
function  prerm:lk($perm)
{
  let $scope := session:get("login")
  where not($scope)
  return
    web:redirect("/")
};

declare 
  %perm:check('/api/v01/', '{$perm}')
function  prerm:api($perm)
{
  let $scope := session:get("login")
  where not($scope)
  return
    web:redirect("/")
};