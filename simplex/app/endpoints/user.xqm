module namespace zt = "zapolnititul2/v1.0/user";
import module namespace sf="http://www.iro37.ru/simplex-framework";

declare 
  %rest:GET
  %rest:path("/lk")
  %output:method("html")
  %output:doctype-public("www.w3.org/TR/xhtml11/DTD/xhtml11.dtd")
function zt:page(){
    sf:tpl('main', map{"body": sf:tpl('user/lk', map{}) })
};


declare
  %rest:GET
  %rest:path("/")
  %output:method("xhtml")
  %output:doctype-public("www.w3.org/TR/xhtml11/DTD/xhtml11.dtd")
function zt:main-page()
{
  sf:tpl('main', map{"body":sf:tpl('public/login', map{})})
};



declare
  %rest:GET
  %rest:path("/logout")
  %output:doctype-public("www.w3.org/TR/xhtml11/DTD/xhtml11.dtd")
function zt:logout()
{
  session:close(),
  web:redirect("/")
};


