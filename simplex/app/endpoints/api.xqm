module namespace api = "zapolnititul2/v1.0/user";
import module namespace sf="http://www.iro37.ru/simplex-framework";


declare 
  %rest:POST
  %rest:path("/api/v01/templates/{$label}/fill")
function api:fillTemplate($label){
  let $data := 
    <table>
      <row id = 'fields'>{
        request:parameter-names() ! (<cell id='{.}'>{request:parameter(.)}</cell>)
      }</row>
    </table>
  
  let $template as xs:base64Binary := 
    sf:lib('s3', 'getObjeckt', map{"label": session:get("id")||"/"||$label})
  
  let $request :=
    <http:request method='post'>
      <http:multipart media-type = "multipart/form-data" >
        <http:header name="Content-Disposition" value= 'form-data; name="template";'/>
        <http:body media-type = "application/octet-stream">{$template}</http:body>
        <http:header name="Content-Disposition" value= 'form-data; name="data";'/>
        <http:body media-type = "application/xml">{$data}</http:body>
      </http:multipart> 
    </http:request> 

  let $response := 
    http:send-request(
      $request,
      'http://zt2-ooxml-1:8984/api/v1/ooxml/docx/template/complete'
    )
  
  let $Content-Disposition := "attachment; filename=" || iri-to-uri($label) || '.docx'
  return
    (
      <rest:response>
          <http:response status="200">
            <http:header name="Content-Disposition" value="{$Content-Disposition}" />
            <http:header name="Content-type" value="application/octet-stream"/>
          </http:response>
        </rest:response>,
      $response[2] => xs:base64Binary()
    )
};

(: получение шаблона из хранилища :)
declare 
  %rest:GET
  %rest:query-param("label", "{$label}")
  %rest:path("/api/v01/templates")
function api:get($label){
  let $Content-Disposition := "attachment; filename=" || iri-to-uri($label) || '.docx'
  return
    (
      <rest:response>
        <http:response status="200">
          <http:header name="Content-Disposition" value="{$Content-Disposition}" />
          <http:header name="Content-type" value="application/octet-stream"/>
        </http:response>
      </rest:response>,
      sf:lib('s3', 'getObjeckt', map{"label": session:get("id")||"/"||$label})
    )
};

(: удаление шалона из хранилища :)
declare 
  %rest:GET
  %rest:query-param("label", "{$label}")
  %rest:path("/api/v01/templates/delete")
  %output:method("xml")
function api:delete($label){
  let $res := 
    for $i in sf:lib('s3', 'getObjecktList', map{"label": session:get("id") || "/" || $label})
    let $url :=
      web:create-url(
        "http://zt2-aws-1:5000/s3/buckets/zt-templates/object",
        map{
          "id": $i
        }
      )
    return
      sf:lib('http.client', 'delete', map{"url": $url})[1]/@status/data() 

  return 
    (
      ("https://zt.titul24.ru/lk?message=" || ($res => distinct-values() => string-join())) => web:redirect()
    )
};

(: загузка шаблона в хранилище :)
declare 
  %rest:POST
  %rest:PUT
  %rest:path("/api/v01/templates")
  %output:method("xml")
function api:post(){
  let $url := 
        web:create-url(
          "http://zt2-aws-1:5000/s3/buckets/zt-templates/object",
          map{
            "id": session:get("id") || "/" || request:parameter("label") || "/" || current-dateTime()
          }
        )
  
  let $url2 := 
        web:create-url(
          "http://zt2-aws-1:5000/s3/buckets/zt-templates/object",
          map{
            "id": "properties/" || session:get("id") || "/" || request:parameter("label") || "/properties.xml" 
          }
        )
  let $args :=
    map{
      "url": $url,
      "content-type": "application/octet-stream",
      "payload": (request:parameter("template") ! map:get(., map:keys(.)[1])) => xs:base64Binary()
    }
  let $args2 :=
    map{
      "url": $url2,
      "content-type": "application/octet-stream",
      "payload": element{"properties"}{element{"status"}{request:parameter("status")}} => serialize() => convert:string-to-base64()
    }
  let $send := 
    try{
      sf:lib('http.client', 'put', $args)
    }catch*{"ошибка"}
  
  let $send2 := 
    try{
      sf:lib('http.client', 'put', $args2)
    }catch*{"ошибка"}

  return
      <a>
        <r>{
          $send[1]/@status/data(),
          $send2[1]/@status/data()
        }</r>
      </a>
};