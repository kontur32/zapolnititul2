(:~ 
 : Данный модуль реализует фукнции http-клиента
 : the features of the Inspection Module.
 :
 : @author   Artel NPO
 : @see      http://docs.basex.org/wiki/XQDoc_Module
 : @version  1.0
 :)

module namespace http.client = "simplex/lib/http/client";

(:~
 : Функция выполняет POST или PUT http-запрос с передачей данных в теле запроса.
 : @param   $arg параметры запроса
 : @return  результат выполнения http-запроса
 :)
declare
  %private
function http.client:post-put($arg as map(*)) as item()* {
  let $len := $arg?payload => convert:binary-to-bytes() => count()
  let $response := 
        <http:request method='{$arg?method}'>
            <http:header name="Content-Length" value="{$len}"/>
            <http:body media-type="application/octet-stream ;charset=UTF-8"/>
        </http:request>
    => http:send-request($arg?url=>xs:anyURI(), $arg?payload)
  return
    $response
};

(:~
 : Функция выполняет POST http-запрос с передачей данных в теле запроса.
 : @param   $arg параметры запроса
 : @return  результат выполнения http-запроса
 :)
declare
  %public
function http.client:post($arg as map(*)) as item()* {
  http.client:post-put(
    map:put($arg, "method", "POST")
  )
};

(:~
 : Функция выполняет PUT http-запрос с передачей данных в теле запроса.
 : @param   $arg параметры запроса
 : @return  результат выполнения http-запроса
 :)
declare
  %public
function http.client:put($arg as map(*)) as item()* {
  http.client:post-put(
    map:put($arg, "method", "PUT")
  )
};

(:~
 : Функция выполняет GET или DELETE http-запрос.
 : @param   $arg параметры запроса
 : @return  результат выполнения http-запроса
 :)
declare
  %private
function http.client:get-delete($arg as map(*)) as item()* {
  <http:request method='{$arg?method}'/> => http:send-request($arg?url)
};

(:~
 : Функция выполняет GET http-запрос.
 : @param   $arg параметры запроса
 : @return  результат выполнения http-запроса
 :)
declare
  %public
function http.client:get($arg as map(*)) as item()* {
   http.client:get-delete(
    map:put($arg, "method", "GET")
  )
};

(:~
 : Функция выполняет DELETE http-запрос.
 : @param   $arg параметры запроса
 : @return  результат выполнения http-запроса
 :)
declare
  %public
function http.client:delete($arg as map(*)) as item()* {
  http.client:get-delete(
    map:put($arg, "method", "DELETE")
  )
};