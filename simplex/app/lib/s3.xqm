module namespace s3 = "zapolnititul2/v1.0/lib/s3";

declare function s3:getObjecktList($args){
  let $list :=
    web:create-url(
        'http://zt2-aws-1:5000/s3/buckets/zt-templates/list',
        map{
            "prefix": $args?label
        }
    ) => fetch:text() => json:parse()
  return
        $list/json/objects/_/id/text() => sort()
};

declare function s3:getObjeckt($args){
  web:create-url(
    "http://zt2-aws-1:5000/s3/buckets/zt-templates/object",
    map{
      "id": s3:getObjecktList($args)[last()]
    }
  ) => fetch:binary()
};